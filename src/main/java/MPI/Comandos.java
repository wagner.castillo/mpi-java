package MPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Wagner Castillo
 */

public class Comandos {
    
    
    public Process comand (String comando){
        Process resulComando = null;
        try {
            String arrayComandos [] = {"bash", "-c", comando};
            resulComando = Runtime.getRuntime().exec(arrayComandos);
        } catch (IOException ex) {
           Logger.getLogger(Comandos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resulComando;
    }
    
    
    public String review(Process proceso){
        String resulUbicacion = "";
        String leerLinea = "";
        try {
            InputStreamReader ingreUbi = new InputStreamReader(proceso.getInputStream());
            BufferedReader archiv = new BufferedReader(ingreUbi);
            leerLinea = archiv.readLine();
            do {
                resulUbicacion += leerLinea+"\n";
                leerLinea = archiv.readLine();
            } while (leerLinea != null);
        } catch (IOException ex) {
            Logger.getLogger(Comandos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resulUbicacion;
    }
    
    
    
}
